/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.services.MessageService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author peti
 */
public class MessageControllerTest {

    private List<Message> mockMessages = new ArrayList<>();
    MessageService mockService;
    
    public MessageControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mockMessages.add(new Message(new Long(1), "peti", "hello"));
        mockMessages.add(new Message(new Long(2), "peti", "szia"));
        
        mockService = Mockito.mock(MessageService.class);
        Mockito.when(mockService.findMessages(Mockito.any(), Mockito.any(), Mockito.any(), 
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(mockMessages);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of showMessages method, of class MessageController.
     */
    @org.junit.Test
    public void testShowMessages() throws Exception {
        //MessageService ms =
        
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new MessageController(null, mockService))
                                 .build();
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/messagetable"))
                .andExpect(MockMvcResultMatchers.view().name("messageList"))
                .andReturn();
        List<Message> returnedMessages = (List<Message>) result.getModelAndView().getModel().get("messages");

    }

    /**
     * Test of showOneMessages method, of class MessageController.
     */
    @org.junit.Test
    public void testShowOneMessages() {
    }

    /**
     * Test of deleteMessages method, of class MessageController.
     */
    @org.junit.Test
    public void testDeleteMessages() {
    }

    /**
     * Test of showCreateMessage method, of class MessageController.
     */
    @org.junit.Test
    public void testShowCreateMessage() {
    }

    /**
     * Test of createMessage method, of class MessageController.
     */
    @org.junit.Test
    public void testCreateMessage() {
    }
    
}
