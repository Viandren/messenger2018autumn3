/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import org.springframework.util.StringUtils;

/**
 *
 * @author peti
 */
public class MessageCreator {

    private static List<Message> messages;
    private static final List<String> authors = new ArrayList<>();
    
    private static final List<String> blabla = new ArrayList<>();
    private static final List<String> signs = new ArrayList<>();
    private static final Random r = new Random();

    public static List<Message> getMessages() {
        return messages;
    }
    
    public static void addMessage(Message m){
        m.setCreateDate(LocalDateTime.now());  
        messages.add(m);
        m.setId(new Long(messages.size()));
    }

    static {
        authors.add("Mézga Aladár");
        authors.add("Mézga Kriszta");
        authors.add("Mézga Géza");
        authors.add("Mézgánél szül. rezovics Paula");
        authors.add("Dr Máris");
        authors.add("MZ/X");
        
        blabla.add("lorem");
        blabla.add("ipsum");
        blabla.add("doloret");
        blabla.add("umque");
        blabla.add("populus");
        blabla.add("usque");
        blabla.add("tandem");
        blabla.add("abutere");
        blabla.add("patienta");
        
        signs.add(".");
        signs.add("!");
        signs.add("?");
        
        messages = LongStream.range(1, 100)
                .mapToObj(i -> {
                    Message m = new Message(i, randomAuthor(), randomText());
                    return m;
                })
                .collect(Collectors.toList());
        System.out.println("messages of size created: " + messages.size());
    }

    private static String randomAuthor() {
        return authors.get(r.nextInt(authors.size()));
    }
    
    private static String randomText(){
        int length = r.nextInt(20)+2;
        return IntStream.range(0, length)
                .mapToObj(i ->  randomSentence())
                .collect(Collectors.joining(" "));
    }
    
    private static String randomSentence(){
        int length = r.nextInt(8)+3;
        String sentence =  IntStream.range(0, length)
                .mapToObj(i ->  blabla.get(r.nextInt(blabla.size())))
                .collect(Collectors.joining(" ", "", signs.get(r.nextInt(signs.size()))));
        return StringUtils.capitalize(sentence);
    }

}
